/**
* Constraint Module
*
* Define action and modalities for MPS communication in a Component Base Architecture
* using Message Passing Asynchronous based communication model
*/
module constraint

open cbsemetamodel
open util/ordering[cbsemetamodel/Tick] as tick
open connectorMPS

/**
Axioms
*/
fact axioms {
	all m:MsgExch, c1:Component, con:Connector, t:Tick|
		intercept[c1,con,m, t] => some c2:Component | injected[c2,con,m,t]
	
	all m:MsgExch, c1:Component, con:Connector, t1:Tick |
		inject[c1,con,m,t1] => some c2:Component, t2:t1.prevs | set_src[c1,m,c2,t2]

	all m:MsgExch, c1:Component, con:Connector, t1:Tick |
		inject[c1,con,m,t1] =>  some c2:Component, t2:t1.prevs| set_rcv[c1,m,c2,t2]

	all m:MsgExch, c:Component, con:Connector, t1:Tick |
		inject[c,con,m,t1] =>  some d:Payload, t2:t1.prevs | set_pld[c,m,d,t2]

	all m:MsgExch, c,s:Component, con:Connector, t:Tick |
		get_src[c,m,s,t] => intercepted[c,con,m,t] and m.sender = s

	all m:MsgExch, c,r:Component, con:Connector, t:Tick |
		get_rcv[c,m,r,t] =>  intercepted[c,con,m, t] and m.receiver = r

	all m:MsgExch, c:Component, con:Connector, d:Payload, t:Tick |
		get_pld[c, m,d,t] =>  intercepted[c,con,m, t] and m.payload = d
}

/** E Modalities */ 
// EInject : a c component have the opportunity to inject a message m at a Tick t
pred E_inject[c:Component, con:Connector,m:MsgExch, t:Tick] {
	one p:c.ports | p in con.connects
}

// EIntercept : a c component have the opportunity to intercept a message m at a Tick t
pred E_intercept[c:Component, con:Connector, m:MsgExch, t:Tick] {
	one p:c.ports | p in con.connects
}

// Esetrcv: a c component have the opportunity to set the declared source of a message m at a Tick t
pred E_set_rcv[c:Component, m:MsgExch, r:Component, t:Tick] {
	one con:Connector | not injected[c,con,m,t]
}

// Esetpld : a c component have the opportunity to set the payload of a message m from a declared source sat a Tick t
pred E_set_pld[c:Component, m:MsgExch, d:Payload, t:Tick] {
	one con:Connector | not injected[c,con,m,t]
}

// Esetsrc : a c component have the opportunity to set the source of a message m at a Tick t
pred E_set_src[c:Component, m:MsgExch, s:Component, t:Tick] {
		one con:Connector | not injected[c,con,m,t]
}

// Egetsrc : a c component have the opportunity to get the source of a message m at a Tick t
pred E_get_src[c:Component, m:MsgExch, s:Component, t:Tick] {
	one con:Connector | intercepted[c,con,m,t]
	m.sender = s
}

// Egetrcv: a c component have the opportunity to get the declared source of a message m at a Tick t
pred E_get_rcv[c:Component, m:MsgExch, r:Component, t:Tick] {
	one con:Connector | intercepted[c,con,m,t]
	m.receiver = r
}

// Egetpld : a c component have the opportunity to get the payload of a message m from a declared source sat a Tick t
pred E_get_pld[c:Component, m:MsgExch, d:Payload, t:Tick] {
	one con:Connector | intercepted[c,con,m,t]
	m.payload = d
}

/** Z Modalities */ 
// Define Authorization that represent an Component  that have a specific Privilege
abstract sig Authorization {
    clnt: one Component,
    addBy: lone Component,
    at: lone Tick
}

sig InjectAuthorization extends  Authorization {}{}
sig InterceptAuthorization extends Authorization {}{}

// Define a list of the Authorization
one sig AuthorizationList {
    list: Authorization -> Tick
}{
	# list.(tick/first) = 0
	all t:Tick - tick/last | let t' = t.next | some a:Authorization, c1,c2:Component |  list.t = list.t' iff not addPrivilege[c1,a,c2,t]
	all a:Authorization | some t:Tick | a in list.t 
}
// Operation to add a Privikege at a specific Tick
pred addPrivilege [c1:Component, a:Authorization,c2:Component,t:Tick] {
     let t' = t.next | AuthorizationList.list.t' = AuthorizationList.list.t + a and c2 = a.clnt  and a.at = t' 
}

// Predicat checking that an Privilge exits at specific Tick
pred hasPrivilege[a:Authorization, t:Tick] {
    a in AuthorizationList.list.t
}

// Predicate that verify that a component c got  Authorization to inject a Msg
pred Z_inject[c:Component, msp:MsgExch,t:Tick] {
    some ac:InjectAuthorization, con:Connector | E_inject[c,con,msp,t] and c =ac.clnt and ac in AuthorizationList.list.t
}

// Predicate that verify that a component c got Authorization to intercept a Msg
pred Z_intercept[c:Component, msp:MsgExch,t:Tick] {
    some ac:InterceptAuthorization, con:Connector | E_intercept[c,con,msp,t] and c in ac.clnt and ac in AuthorizationList.list.t
}


// Predicate that verify that a component c got  Authorization to inject a Msg added by c2
pred Z_inject[c1:Component, c2:Component, msp:MsgExch,t:Tick] {
    some ac:InjectAuthorization, con:Connector | E_inject[c1,con,msp,t] and ac.clnt = c1 and ac.addBy = c2 and ac in AuthorizationList.list.t
}

// Predicate that verify that a component c got Authorization to intercept a Msg added by c2
pred Z_intercept[c1:Component, c2:Component, msp:MsgExch,t:Tick] {
    some ac:InterceptAuthorization, con:Connector | E_intercept[c1,con,msp,t] and ac.clnt = c1 and ac.addBy = c2 and ac in AuthorizationList.list.t
}


/** 
	Actions 
*/
// inject : a component c added a message m into the system at a tick  t
pred inject[c1:Component, con:Connector, m:MsgExch, t:Tick] {
	E_inject[c1,con,m,t]
	some c2:Component | c1.send[c2,m,t]
}

// intercept: a component got the message m from the system at a tick t
pred intercept[c1:Component,con:Connector, m:MsgExch, t:Tick] {
	E_intercept[c1,con,m,t]
	some c2:Component | c1.receive[c2,m,t]
}

// set_src : a component set the  declared source s from the message m at a tick t
pred set_src[c:Component,m:MsgExch, s:Component, t:Tick] {
	E_set_src[c,m,s,t]
	m.sender = s 
}

//  set_rcv : a component set  the receiver(s) r from the message m at a tick t
pred set_rcv[c:Component, m:MsgExch, r:Component, t:Tick] {
	E_set_rcv[c,m,r,t]
	m.receiver = r
}

//  set_pld : a component set the payload d from the message m at a tick t
pred set_pld[c:Component, m:MsgExch, d:Payload, t:Tick] {
	E_set_pld[c,m,d,t]
	m.payload = d
}

// get_src : a component got the  declared source s from the message m at a tick t
pred get_src[c:Component,m:MsgExch, s:Component, t:Tick] {
	E_get_src[c,m,s,t]
}

//  get_rcv : a component got the receiver(s) r from the message m at a tick t
pred get_rcv[c:Component, m:MsgExch, r:Component, t:Tick] {
	E_get_rcv[c,m,r,t]
}

//  get_pld : a component got the payload d from the message m at a tick t
pred get_pld[c:Component, m:MsgExch, d:Payload, t:Tick] {
	E_get_pld[c,m,d,t]
}

/** Macros */
// injected : a component c added a message m into the system before a tick  t
pred injected[c:Component,con:Connector,m:MsgExch, t1:Tick] {
	one t2:t1.prevs | inject[c,con,m,t2]
}

// intercepted : a component got the message m from the system before a tick t
pred intercepted[c:Component,con:Connector,m:MsgExch, t1:Tick] {
	one t2:t1.prevs | intercept[c,con,m,t2]
}

// sent_by : a c component injected a message m and c is the accurate origin of m m.payload
pred sent_by[m:MsgExch,c:Component, t:Tick] {
	one con:Connector | injected[c,con,m,t] and m.sentBy = c
}

// sent_with : a component c injected a message m that contains a paylaod d
pred sent_with[m:MsgExch, d:Payload, t:Tick] {
	one c:Component | sent_by[m,c,t] and m.sender = c and m.payload = d 
}

// sent_to : some c component injected a message m with r has intended receivers
pred sent_to[m:MsgExch,r:set Component, t:Tick] {
	one c:Component | sent_by[m,c,t] and r = m.receiver
}

