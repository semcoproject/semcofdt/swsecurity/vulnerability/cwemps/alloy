/**
* Property Module
*
* Property abstraction  for Component Base Architecture
* and Message Passing Asynchronous based communication model
*/
module property

open cbsemetamodel

abstract sig Property {}

// Abstract property on component 
abstract sig ComponentProperty extends  Property {
	c: one Component,
	m: one Interaction
}

pred ComponentProperty.holds[c1:one Component,msg: one Interaction] {
	{ some p:this { p.c = c1 and p.m = msg }}
}

// Abstract property representing a threat on communication between two component using MPS communication
abstract sig ConnectorProperty  extends  Property{
	con: one Connector,
	m: one Interaction
}

fun ConnectorProperty.c1: one Component{
	{ c:Component | c.ports in this.con.connects }
}

fun ConnectorProperty.c2: one Component{
	{ c:Component |  c.ports in this.con.connects }
}

pred ConnectorProperty.holds[conn:one Connector, msg: one  Interaction] {
	{ this.con = conn and this.m = msg and msg.connector = conn }
}
